# Repositório para sediar o runner do renovate bot.

Veja mais em [aqui](https://gitlab.com/renovate-bot/renovate-runner/);

### Variáveis de ambiente em uso:

- `RENOVATE_TOKEN`: Token do gitlab com `read_user`, `api` e `write_repository` permissões.
- `GITHUB_COM_TOKEN`: Token do github para o bot não tomar timeout durante requests de changelog das libs e etc.
